import asyncio
import time
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

desc='algo'
changeVar = 'algo'
n=0
cliente1 = None
class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        if self.message == '{"type": "bye"}':
            print('Send:', self.message)
            self.transport.sendto(self.message.encode())
        else:
            registerMessage= 'REGISTER CLIENT'
            print('Send:', registerMessage)
            self.transport.sendto(registerMessage.encode())


    def datagram_received(self, data, addr):
        global n
        if data.decode() != 'OK'and n==0:
            global changeVar
            changeVar = data.decode()
            n = n + 1
            print("Received:", data.decode())

        elif data.decode() == 'OK' and self.message != '{"type": "bye"}' :
            print("Received:", data.decode())
            print('Send:', self.message)
            self.transport.sendto(self.message.encode())

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def wait_for_variable_change():
    global changeVar
    while changeVar == 'algo':
        await asyncio.sleep(5)

async def wait_for_remote_description(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)
async def consume_signaling(pc, message):

    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    global cliente1
    BYE = {"type": "bye"}
    mensajeAdios = None

    if message == desc:
        cliente1 = EchoClientProtocol(message, on_con_lost)
        transport, protocol = await loop.create_datagram_endpoint(
            lambda: cliente1, remote_addr=('127.0.0.1', 6789))

    elif message == BYE:
        cliente1.transport.sendto(json.dumps(message).encode())
        print('Closing client data ....')
        cliente1.transport.close()
        mensajeAdios = BYE

        #print('Send:', BYE)
        #byeStr = json.dumps(mensajeAdios)
        #print(byeStr)
        #print(type(byeStr))
        #cliente.transport.sendto(byeStr.encode())

    while True:

        await wait_for_variable_change()
        global changeVar
        answer = json.loads(changeVar)
        sdp = answer['sdp']
        tipo= answer['type']
        obj = RTCSessionDescription(sdp, tipo)
        if isinstance(obj, RTCSessionDescription) and mensajeAdios != BYE:
            print('Waiting for a new connection')
            await pc.setRemoteDescription(obj)
            await wait_for_remote_description(pc)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())

        elif mensajeAdios == BYE:
            print("Exiting")
            break
    quit()


time_start = None


def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc):
    # create data channel
    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        count= 0
        BYE = {"type": "bye"}
        while True:
            if count == 3:
                print('Closing channel...')
                channel.close()
                break

            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            count = count + 1
            await asyncio.sleep(1)
        await consume_signaling(pc, BYE)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())

    offer_dict = pc.localDescription.__dict__
    offer_str = json.dumps(offer_dict)
    global desc
    desc = offer_str
    await consume_signaling(pc, offer_str)


if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_offer(pc)

    # run event loop
    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(coro)

    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
