import asyncio
import json
import sdp_transform


register={}
class ServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if message == "REGISTER CLIENT" or message == "REGISTER SERVER":
            if message == "REGISTER CLIENT":
                register['CLIENT'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
            if message == "REGISTER SERVER":
                register['SERVER'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
            print(register)
        else:
            if addr == register['CLIENT'] :
                mensajeOferta = message
                print('Send: ', mensajeOferta)
                try:
                    self.transport.sendto(message.encode(), register['SERVER'])
                except:
                    print('Waiting for register server...')

                #self.transport.sendto('message'.encode(), register['SERVER'])

            else:
                mensajeAnswer = message
                print('Send: ', mensajeAnswer)
                self.transport.sendto(message.encode(), register['CLIENT'])



        ''' 
        else:
            signal[addr] = message
            print(signal)
            with open("server_data_udp.sdp", "r") as f:
                data=f.read()
                print()
                print(data)
            w = sdp_transform.parse(data)
            sdpData = sdp_transform.write(w)
            print(sdpData)
            json_data = {
                "sdp": sdpData,
                "type": "answer"}  # transform sdp text to json
            #mensaje = json.dumps(json_data)
            message= {"sdp": "v=0\r\no=- 3909166029 3909166029 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic:WMS *\r\nm=application 36144 DTLS/SCTP 5000\r\nc=IN IP4 192.168.1.52\r\na=mid:0\r\na=sctpmap:5000 webrtc-datachannel 65535\r\na=max-message-size:65536\r\na=candidate:0c586a64e390f21cf3ff7ed0f9c40dce 1 udp 2130706431 192.168.1.52 36144 typ host\r\na=candidate:d0c5f0df5e7857be43453449e17d897e 1 udp 1694498815 87.220.178.37 36144 typ srflx raddr 192.168.1.52 rport 36144\r\na=end-of-candidates\r\na=ice-ufrag:jTO1\r\na=ice-pwd:nhp2eoStBNgdT92d2JNifQ\r\na=fingerprint:sha-256 42:FE:93:CC:F6:57:91:E5:EA:A5:B2:A7:A1:B9:38:26:2F:7B:D4:67:8A:22:BF:CE:44:53:0F:CD:55:8D:CE:76\r\na=setup:active\r\n", "type": "answer"}
            mensaje = json.dumps(message)
            self.transport.sendto(mensaje.encode(), addr)'''



    #register[addr] = message
        #print(register)
        #print('Send %r to %s' % (message, register[message]))
        #self.transport.sendto(data, register[message])


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ServerProtocol(),
        local_addr=('127.0.0.1', 6789))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())