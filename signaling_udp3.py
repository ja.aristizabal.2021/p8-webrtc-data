import asyncio
import json
import sdp_transform


register={}
clients = {}
numClient = 0
mensajeAnswer = ''
class ServerProtocol():
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if message == "REGISTER CLIENT" or message == "REGISTER SERVER":
            if message == "REGISTER CLIENT":
                register['CLIENT'] = addr
                global numClient
                numClient = numClient + 1
                clients[f'CLIENT({numClient})'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
                if 1 < numClient:
                    self.transport.sendto('WAKE-UP'.encode(), register['SERVER'])


            elif message == "REGISTER SERVER":
                register['SERVER'] = addr
                registerMessage = 'OK'
                self.transport.sendto(registerMessage.encode(), addr)
            print(register)
            print(clients)

        elif message == "{'type': 'bye'}":
            if addr == register['CLIENT']:
                print(f'Send to ', register['SERVER'], ':', message)
                self.transport.sendto(message.encode, register['SERVER'])
            else:
                print(f'Send to ', register['CLIENT'], ':', message)


        else:
            global mensajeAnswer
            if addr == register['CLIENT'] and message != "{'type': 'bye'}" :
                mensajeOferta = message
                print('Send: ', mensajeOferta)
                try:
                    self.transport.sendto(message.encode(), register['SERVER'])
                    if 1 < numClient:
                        self.transport.sendto(message.encode(), register['SERVER'])
                        #self.transport.sendto(mensajeAnswer.encode(), register['CLIENT'])
                except:
                    print('Waiting for register server...')

                #self.transport.sendto('message'.encode(), register['SERVER'])

            elif addr == register['SERVER'] and message != "{'type': 'bye'}"  :
                mensajeAnswer = message
                print('Send: ', mensajeAnswer)
                self.transport.sendto(message.encode(), register['CLIENT'])


async def main():
    print("Starting UDP server")

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    # One protocol instance will be created to serve all
    # client requests.
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ServerProtocol(),
        local_addr=('127.0.0.1', 6789))

    try:
        await asyncio.sleep(3600)  # Serve for 1 hour.
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())