import asyncio
import json
import time

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling

changeVar = 'algo'
n = 0
adios = None
BYE = {"type": "bye"}
condition = None
mensajeRecibido = None
desc= 'algo'
cliente = ''

class EchoClientProtocol:
    def __init__(self, message, on_con_lost):
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.message)
        self.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        global changeVar
        print("Received:", data.decode())
        if data.decode() != 'OK' and changeVar == 'algo':
            changeVar = data.decode()
        if data.decode() == json.dumps(BYE):
            global adios, condition
            adios = json.loads(data.decode())
            condition = BYE
        if 'WAKE-UP' in data.decode():
            global mensajeRecibido
            mensajeRecibido= data.decode()
            self.transport.sendto(desc.encode(), addr)
            print()
            print('Trying new connection')

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def wait_for_variable_change():
    while changeVar == 'algo':
        await asyncio.sleep(10)
async def wait_for_remote_description(pc):
    while not pc.remoteDescription is None:
        await asyncio.sleep(1)


async def consume_signaling(pc):
    loop = asyncio.get_running_loop()
    on_con_lost = loop.create_future()
    message = "REGISTER SERVER"

    cliente = EchoClientProtocol(message, on_con_lost)
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: cliente, remote_addr=('127.0.0.1', 6789))
    while True:
        await wait_for_variable_change()
        offer = json.loads(changeVar)
        sdpOffer = offer['sdp']
        typeOffer = offer['type']
        obj = RTCSessionDescription(sdpOffer, typeOffer)
        if isinstance(obj, RTCSessionDescription) and adios==None :
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                offer_dict = pc.localDescription.__dict__
                offer_str = json.dumps(offer_dict)
                global desc
                desc = offer_str
                print('Send:', offer_str)
                cliente.transport.sendto(offer_str.encode())  # Send offer to UDP server
                await wait_for_remote_description(pc)

        elif adios == {"type": "bye"}:
            print('Received BYE signal. Closing connection.')
            await pc.close()
            break  # salir del bucle


time_start = None


async def run_answer(pc):

        @pc.on("datachannel")
        def on_datachannel(channel):
            print(f"channel({channel.label}) > created by remote party")

            @channel.on("message")
            def on_message(message):

                print(f"channel({channel.label}) > {message}")

                if isinstance(message, str) and message.startswith("ping"):
                    # reply
                    message = f"pong{message[4:]}"
                    print(f"channel({channel.label}) > {message}")
                    channel.send(message)


        await consume_signaling(pc)




if __name__ == "__main__":
    pc = RTCPeerConnection()
    coro = run_answer(pc)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())

